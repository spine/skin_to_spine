# Skin to Spine
This is the repository associated to the publication: *3D inference of the scoliotic spine from depth maps of the back* by [Nicolas Comte](https://cometicon.github.io), [Sergi Pujades](http://sergipujades.free.fr/), Aurélien Couvoisier, Olivier Daniel, [Jean-Sébastien Franco](http://morpheo.inrialpes.fr/people/franco/), François Faure, [Edmond Boyer](https://morpheo.inrialpes.fr/people/Boyer/) (2024).
 
In: Skalli, W., Laporte, S., Benoit, A. (eds) Computer Methods in Biomechanics and Biomedical Engineering II. CMBBE 2023. Lecture Notes in Computational Vision and Biomechanics, vol 39. Springer, Cham. https://doi.org/10.1007/978-3-031-55315-8_18


## Abstract
Recent advances combining outer images and deep-learning algorithms (DLA) show promising results in the detection and the characterization of the Adolescent Idiopathic Scoliosis (AIS). However, these methods are providing a limited 2D characterization while scoliosis is defined in 3D. In this study we propose an inference method that takes as input a depth map of the back of a person and outputs the 3D shape estimation of the thoracolumbar spine. Our DLA method predicts 3D vertebrae positions with an average 3D error of 7.1mm (std: 4.7mm). From the predicted 3D positions, scoliosis can be located and estimated with a mean absolute error (MAE) of 5.5◦ (std: 6.2◦) in the frontal plane. Moreover, sagittal alignments can be estimated with a MAE of 6.4◦ (std: 5.5◦) in kyphosis and 8.3◦ (std: 6.8◦) in lordosis. In addition, our non-ionizing approach can detect scoliosis with an accuracy of 89%.


## Download the repository
```bash
git clone https://gitlab.inria.fr/spine/skin_to_spine.git
```


## Environment setup
The code has been tested with python 3.8, virtualenv and pip.

Requirements are listed in the *requirement.txt* file. The packages can be installed in a virtual environment with the following command line:

```bash
pip install -r requirements.txt
```

In case of error with PyTorch, install a version fitting your cuda platform version. See: [https://pytorch.org/get-started/locally](https://pytorch.org/get-started/locally)


## Demo
A jupyter notebook *inference.ipynb* introduce a code example using pretrained CNN and PCA models.
These models have been trained with subjects of the [NMDID (New Mexico Decedent Image Database)](https://nmdid.unm.edu/) and patients from Grenoble Hospitals<sup>1</sup>.
The lists of subject ids used for training processes are included in directories corresponding to the models.

An image example is provided with courtesy of [Anatoscope](https://www.anatoscope.com/)<sup>2</sup> for testing.
This is a depth map captured from a virtual avatar in upright position with its associated 3D vertebra positions.
Only two vertebra positions (related to T01 and L05) can be given in the corresponding file without depth information (z value = 0).

Note: input depth maps should be generated according to an orthographic camera.


## Hardware
The code has been tested on: 
- Ubuntu 18.04 with RTX  Quadro 5000 8Gb, cuda 11.3
- Ubuntu 20.04 with GTX 1050 2Gb, cuda 11.4


## Contact
For any question about the paper and repository, please contact [Nicolas Comte](mailto:nicolas.comte@inria.fr) and [Sergi Pujades](mailto:sergi.pujades-rocamora@inria.fr)


## License
The trained models and the code are provided under [CC BY-NC-SA 4.0 license](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode).


## Citation (Bibtex)
```
@InProceedings{Comte2024,
author="Comte, Nicolas
and Pujades, Sergi
and Courvoisier, Aur{\'e}lien
and Daniel, Olivier
and Franco, Jean-S{\'e}bastien
and Faure, Fran{\c{c}}ois
and Boyer, Edmond",
editor="Skalli, Wafa
and Laporte, S{\'e}bastien
and Benoit, Aur{\'e}lie",
title="3D Inference of the Scoliotic Spine from Depth Maps of the Back",
booktitle="Computer Methods in Biomechanics and Biomedical Engineering II",
year="2024",
publisher="Springer Nature Switzerland",
address="Cham",
pages="159--168",
abstract="Recent advances combining outer images and deep-learning algorithms (DLA) show promising results in the detection and the characterization of the Adolescent Idiopathic Scoliosis (AIS). However, these methods are providing a limited 2D characterization while scoliosis is defined in 3D. In this study we propose an inference method that takes as input a depthmap of the back of a person and outputs the 3D shape estimation of the thoracolumbar spine. Our DLA method predicts 3D vertebrae positions with an average 3D error of 7.1 mm (std: 4.7 mm). From the predicted 3D positions, scoliosis can be located and estimated with a mean absolute error (MAE) of 5.5{\$}{\$}^{\backslash}circ {\$}{\$}∘(std: 6.2{\$}{\$}^{\backslash}circ {\$}{\$}∘) in the frontal plane. Moreover, sagittal alignments can be estimated with a MAE of 6.4{\$}{\$}^{\backslash}circ {\$}{\$}∘(std: 5.5{\$}{\$}^{\backslash}circ {\$}{\$}∘) in kyphosis and 8.3{\$}{\$}^{\backslash}circ {\$}{\$}∘(std: 6.8{\$}{\$}^{\backslash}circ {\$}{\$}∘) in lordosis. In addition, our non-ionizing approach can detect scoliosis with an accuracy of 89{\%}.",
isbn="978-3-031-55315-8"
}
```

## Acknowledgments
The authors acknowledge the support of the Association Nationale de la Recherche et de la Technologie (ANRT) with CIFRE No.2019/1197 and the Agence Nationale de la Recherche (ANR) with JCJC SEMBA ANR-19-CE23-0003.


<sup>1</sup> Edgar, HJH; Daneshvari Berry, S; Moes, E; Adolphi, NL; Bridges, P; Nolte, KB (2020). New Mexico Decedent Image Database. Office of the Medical Investigator, University of New Mexico. doi.org/10.25827/5s8c-n515.
<sup>2</sup> &copy; 2023 Anatoscope
