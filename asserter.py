from __future__ import absolute_import
import os
import sys
import inspect
import numpy as np

FLOAT_TOLERANCE = 1e-8

def _equality(a, b, float_tolerance=FLOAT_TOLERANCE):
    if isinstance(a, float) or isinstance(b, float):
        return float(abs(a-b)) < float_tolerance
    else:
        return a == b


def _greater(a, b):
    return a > b


def _lower(a, b):
    return _greater(b, a)


def _diff(a, b, float_tolerance=FLOAT_TOLERANCE):
    return not(_equality(a, b, float_tolerance=float_tolerance))


def _all_close(a, b, float_tolerance=FLOAT_TOLERANCE):
    return np.allclose(a, b, atol=float_tolerance)


def _path_exists(path):
    return os.path.exists(path)


def _is_none(e):
    return e is None


def make_assert(condition, message_if_false, out=sys.stderr, fatal=True):
    try:
        assert condition, message_if_false

    except AssertionError as msg:
        out.write(str(msg) + os.linesep)
        if fatal:
            sys.exit(1)


def assert_path_exists(path, out=sys.stderr):
    frame, filename, line_number, function_name, lines, index = inspect.stack()[1]
    if path != "":
        message = "Error at line {} of {}: {} does not exist.".format(line_number, filename, path)
    else:
        message = "Error at line {} of {}: path is empty, got '{}'.".format(line_number, filename, path)
    make_assert(_path_exists(path), message, out)


def assert_equality(a, b, tol=1e-8, out=sys.stderr):
    message = "Error: {} != {}.".format(str(a), str(b))
    make_assert(_equality(a, b, tol), message, out)


def assert_allclose(v0, v1, float_tolerance=FLOAT_TOLERANCE, out=sys.stderr):
    message = "Error: {} is different from {}".format(v0, v1)
    make_assert(_all_close(v0, v1, float_tolerance=float_tolerance), message, out)
