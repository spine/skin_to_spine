import os

def pandasPositionsToDict(df, scale, idField="marker", xField="x", yField="y", zField="z"):
    res = dict()
    for index, row in df.iterrows():
        position = np.array([row[xField], row[yField], row[zField]]) * scale
        res[row[idField]] = position.tolist()

    return res

def dictToOrderedList(d: dict, keys: list):
    ll = list()
    for k in keys:
        assert k in d.keys()
        ll.append(d[k])
    return ll


def getPositionsFromCSV(csv_path, scale, idField="marker", xField="x", yField="y", zField="z"):
    import pandas as pd

    positions_csv = pd.read_csv(csv_path)

    return pandasPositionsToDict(positions_csv, scale, idField, xField, yField, zField)

def findExpression(pattern: str, text: str) -> (list, list):
    """Find expression from pattern in an input string returns -> list, list"""
    import re
    expr = list()
    bounds = list()
    for match in re.finditer(pattern, text):
        expr.append(match.group())
        bounds.append(match.span())
    return expr, bounds


def hasExpression(pattern: str, text: str) -> bool:
    expr, bounds = findExpression(pattern, text)
    return len(expr) > 0


def getFilename(filepath: str, extension: str) -> str:
    if filepath[-1] == os.path.sep:
        filepath = filepath[:-1]
    basename = os.path.basename(filepath)
    _, match_pos = findExpression(".{}".format(extension), basename)
    if not len(match_pos) > 0:
        raise ValueError("No match in \'{}\' with \'{}\'".format(filepath, extension))
    return basename[:match_pos[0][0]]