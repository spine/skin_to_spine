import numpy as np

device = "cuda:0"
POSITIONS_NDIM=3
TRANSLATION_AUGMENTATION = False
ONLY_NMDID = False
DEEP_VALUE = np.nan

DEFAULT_NIFTI_FILENAME = "data.nii.gz"

DEFAULT_ROTATIONS_AUGMENTATION_LIMIT = [(-5, 6), (-10, 11), (-5, 6)]

VERTEBRA_IDS = ["Atlas", "Axis", "C03", "C04", "C05", "C06", "C07",
                "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", "T09", "T10", "T11", "T12",
                "L01", "L02", "L03", "L04", "L05",
                "Sacrum"]

VERTEBRA_OF_INTEREST_IDS = ["T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", "T09", "T10", "T11", "T12",
                "L01", "L02", "L03", "L04", "L05"]

VERTEBRA_REFERENCE = 8
