import numpy as np
import random
import sys
import os
import torch
import torchvision.transforms.functional as TF
from PIL import Image
from torch.utils.data import Dataset
import torch.nn.functional as F
import ctypes

libgcc_s = ctypes.CDLL('libgcc_s.so.1')

sys.path.insert(0, os.path.abspath(os.path.join('..', '..')))
from depthmap_generation import depthmaps as dms
import tools
import constants

VERTEBRA_REFERENCE = constants.VERTEBRA_REFERENCE


def depthmap_treatment(depthmap, image_size, depth_translation="min", image_crop=(0.0, 0.0), relative_vertebra_depth=True, binary_images=False):
    """Transform depthmap data to infos for training and testing. Warning, image_crop is relative to the resulting depthmap size"""
    # get image and positions in ROI
    roi = depthmap.roi_square_spine(t01l05_margin=0.1)

    depthmap_size = depthmap.values.shape

    # change crop according to parameters (make translation)
    effective_image_crop = [0., 0.]
    effective_image_crop[0] = (depthmap_size[0]/image_size[0]) * image_crop[0]
    effective_image_crop[1] = (depthmap_size[1]/image_size[1]) * image_crop[1]

    roi = [i for i in roi]

    roi[0] += np.round(effective_image_crop)[0]
    roi[1] += np.round(effective_image_crop)[1]
    roi[2] += np.round(effective_image_crop)[0]
    roi[3] += np.round(effective_image_crop)[1]

    roi = [int(i) for i in roi]

    img, positions = depthmap.crop_to_roi(roi, reshape=False)

    img_previous_shape = img.shape

    # starting normalizations of the resulting image and associated positions
    # retrieve applied scale on the image
    dscale = depthmap.scale  # mm/pix
    dlen = np.linalg.norm(depthmap.centroids[-1][:2] - depthmap.centroids[0][:2])  # pix.
    ilen = np.linalg.norm(positions[-1][:2] - positions[0][:2])  # pix. in crop image
    tscale = ilen / dlen  # pixc/pix, after crop ; should be equal to 1 since there is no scaling

    scaling_factor = dscale * 1./tscale  # mm/pix * pix/pixc (scale pix -> mm)

    # normalize 2D positions between 0-1
    positions[:, :2] = positions[:, :2] / np.array(img.shape[::-1])

    # shift back against the camera (default)
    if depth_translation == "min":
        depth_translation = np.nanmin(img)
    # or shift according to a given value
    img -= depth_translation

    # apply the change to the depth values
    if depthmap.centroids.shape[1] == 3:
        positions[:, 2] -= depth_translation

        # then, normalize last dimension within scaling factor applied in the 2 others
        positions[:, 2] *= (1. / scaling_factor)  # mm -> pix. values
        positions[:, 2] /= img.shape[0]  # same normalization to [0, 1]

        # finally define depth dimension according to the 8th vertebra
        if relative_vertebra_depth:
            if len(positions) == 17:
                positions[:, 2] -= positions[VERTEBRA_REFERENCE, 2]

    # test
    positions_rescaled = positions * img.shape[0] * scaling_factor #norm -> pix -> mm
    spine_3d_xrange_rescaled = np.diff(tools.minmax(positions_rescaled[:, 0]))
    spine_3d_yrange_rescaled = np.diff(tools.minmax(positions_rescaled[:, 1]))
    spine_3d_zrange_rescaled = np.diff(tools.minmax(positions_rescaled[:, 2]))

    spine_3d_xrange_dm = np.diff(tools.minmax(depthmap.centroids[:, 0])) * depthmap.scale #pix -> mm
    spine_3d_yrange_dm = np.diff(tools.minmax(depthmap.centroids[:, 1])) * depthmap.scale
    spine_3d_zrange_dm = np.diff(tools.minmax(depthmap.centroids[:, 2]))

    assert np.allclose(spine_3d_xrange_rescaled, spine_3d_xrange_dm)
    assert np.allclose(spine_3d_yrange_rescaled, spine_3d_yrange_dm)
    assert np.allclose(spine_3d_zrange_rescaled, spine_3d_zrange_dm)

    # set pixel value to the background
    background_value = np.nanmax(img)
    img = np.where(np.isnan(img), background_value, img)

    # rescale depth to [0-1]
    # img /= np.max(img)

    if binary_images:
        min, max, mean = np.min(img), np.max(img), np.mean(img)
        img = 1. - (img < (max)).astype(np.float_)
        elements_between_min_and_max = np.where(np.logical_and(img > (np.min(img) + 1.e-5), img < (np.max(img) - 1.e-5)))
        assert len(img[elements_between_min_and_max]) == 0

    # rescale depths to [-1, 1] as suggested in deeprior
    img = tools.normalizeImgAB(img, min=-1., max=1.)

    # finally transform img to the expected size
    img = np.asarray(Image.fromarray(img).resize(image_size, Image.Resampling.BILINEAR))


    return img.astype(np.double), positions.astype(np.double)


def case_to_depthmaps(case_id, case_dir, image_size, ndim, translations=[(0.0, 0.0)], binary_images=False):
    case_collection = dms.Case.from_dir(case_dir, case_id, ndim=ndim)

    case_images = list()
    case_landmarks = list()
    case_scales = list()

    for d in case_collection.back_depthmaps:
        o_spine_len_mm = np.linalg.norm(np.array(d.centroids[-1][:2]) - np.array(d.centroids[0][:2])) * d.scale
        for translation in translations:
            img, positions = depthmap_treatment(d, image_size=image_size, image_crop=translation, binary_images=binary_images)

            p_spine_len_pix = np.linalg.norm(np.array(positions[-1][:2]) - np.array(positions[0][:2]))*image_size[0]

            current_scale = o_spine_len_mm / p_spine_len_pix

            case_images.append(img)
            case_landmarks.append(positions)
            case_scales.append(current_scale)

    return case_images, case_landmarks, case_scales


class Depth_dataset(Dataset):
    def __init__(self, imgs: list, landmarks: list, ids=None, scales=None):
        self.depthmaps = imgs
        self.positions = np.array(landmarks)
        self.ids = ids
        self.scales = scales

    @staticmethod
    def get_centroids(positions):
        if len(positions) > 17:
            return positions[0::4]
        return positions[:]


    def compute_max_ca(self, index):
        positions = np.array(Depth_dataset.get_centroids(self.positions[index])) * np.float64(self.depthmaps[index].shape[0])
        res = 0.0
        try:
            res =  self._scoliosis_severity(positions)
        except Exception as inst:
            res = 0.0
        return res

    def __len__(self):
        return len(self.depthmaps)

    def __getitem__(self, index):
        # get elements
        img, landmarks = self.getNonTensor(index)

        img = TF.to_tensor(Image.fromarray(img))
        landmarks = torch.tensor(landmarks)

        return img, landmarks

    def getNonTensor(self, index):
        # transform to tensors
        img, landmarks = self.depthmaps[index], Depth_dataset.get_centroids(self.positions[index])
        if not self.transform is None:
            # use online augmentation within possible offline augmentation
            img, landmarks = self.transform.getNonTensor(img, landmarks)
        return img, landmarks
