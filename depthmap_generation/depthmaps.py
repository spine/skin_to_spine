from typing import Any
import numpy as np
import sys
import os
import json
from matplotlib import pyplot as plt
from PIL import Image

sys.path.insert(0, os.path.abspath(os.path.join('..')))
import asserter
import commons
import constants
import pathTools

DEEP_VALUE = constants.DEEP_VALUE

# File patterns for depthamps, related vertebrae positions and image scale.
ARRAY_FILE_REGEX = "{}_[_\-0-9a-zA-Z]*_array.npy$"


def get_infos_from_depth_file(path: str):
    filename = commons.getFilename(path, "npy")
    elements = filename.split("_")

    patientId = elements[0]
    d = 1
    if patientId == "patient":
        d = 2
        patientId = "_".join(elements[:d])

    assert "depthmap" == elements[d]
    assert "back" == elements[d + 1] or "profile" == elements[d + 1]

    return patientId


def load_depth(path: str) -> np.array:
    asserter.assert_path_exists(path)
    return np.load(path)


def load_centroids_dict(path: str):
    asserter.assert_path_exists(path)
    with open(path, "r") as f:
        json_string = json.load(f)
        if isinstance(json_string, str):
            return json.loads(json_string)
        return json_string


def load_centroids_array(path: str, ndim=constants.POSITIONS_NDIM) -> np.array:
    """Return an ordered array of T01-L05 vertebra locations on corresponding depth image"""
    values = load_centroids_dict(path)
    res = list()
    for vertId in constants.VERTEBRA_OF_INTEREST_IDS:
        if vertId in values.keys():
            res.append(values[str(vertId)][:ndim])
    return np.array(res)


class Depthmap():
    """
    A single dephtmap:
    * image: patientID_depthmap_back_array.npy (numpy array of size n x n).
    * centroids: patientID_depthmap_back_centroids.json (dict; key: vertebra ID; value: list)
    * image scale: patientID_depthmap_back_scale.json (numpy array of size 2)
    """
    def __init__(self):
        self.array_path = ""
        self.values = np.array([])
        self.case_id = ""
        self.centroids_path = ""
        self.scale = None  # mm/pix
        self.scale_path = ""
        self.centroids = np.array([])
        self.mirror = False
        return

    @staticmethod
    def from_npy(npy_path: str, ndim=constants.POSITIONS_NDIM):
        """
        npy_path: path to the depth map file (npy format)
        """
        d = Depthmap()
        d.set_from_npy(npy_path, ndim=ndim)
        return d

    @staticmethod
    def from_files(case_id: str, img_path: str, pos_path: str, scale_path: str, ndim=constants.POSITIONS_NDIM):
        """
        npy_path: path to the depth map file (npy format)
        """
        d = Depthmap()
        d.set_from_files(case_id, img_path, pos_path, scale_path, ndim=ndim)
        return d

    def set_from_npy(self, npy_path, ndim=constants.POSITIONS_NDIM):
        """
        npy_path: path to the depth map file (npy format)
        """
        asserter.assert_path_exists(npy_path)
        self.array_path = npy_path
        self.values = load_depth(self.array_path)
        self.case_id = get_infos_from_depth_file(npy_path)

        self.centroids_path = npy_path.replace("array.npy", "centroids.json")
        asserter.assert_path_exists(self.centroids_path)
        self.centroids = np.array(load_centroids_array(self.centroids_path, ndim=ndim))

        self.scale_path = npy_path.replace("array.npy", "scale.npy")
        self.scale = float(np.load(self.scale_path))

        self.mirror = False

    def set_from_files(self, case_id: str, img_path: str, pos_path: str, scale_path: str, ndim=constants.POSITIONS_NDIM):
        """
        img_path: path to the depth map in npy format
        pos_path: path to vertebrae 3D positions (T01 and L05 at least, last dimension can be set to 0)
        scale_path: path to pixel sale in the image in npy format
        """
        asserter.assert_path_exists(scale_path)
        asserter.assert_path_exists(img_path)
        asserter.assert_path_exists(pos_path)

        self.array_path = img_path
        self.values = load_depth(self.array_path)
        self.case_id = case_id

        self.centroids_path = pos_path
        asserter.assert_path_exists(self.centroids_path)
        self.centroids = np.array(load_centroids_array(self.centroids_path, ndim=ndim))

        self.scale_path = scale_path
        self.scale = float(np.load(self.scale_path))

        self.mirror = False

    def depth_range(self):
        return [np.nanmin(self.values), np.nanmax(self.values)]

    def _roi(self):
        """Returns minimal ROI box that contains torso within centroids -> xmin, ymin, xmax, ymax"""
        xmin_centroids, ymin_centroids = np.min(self.centroids[:, :2], axis=0)
        xmax_centroids, ymax_centroids = np.max(self.centroids[:, :2], axis=0)

        background_indices = np.argwhere(~np.isnan(self.values[int(ymin_centroids):int(ymax_centroids) + 1, :]))
        _, xmin_img = np.min(background_indices, axis=0)
        _, xmax_img = np.max(background_indices, axis=0)

        return int(xmin_img), int(np.floor(ymin_centroids)), int(xmax_img), int(np.ceil(ymax_centroids))

    def roi(self, t01l05_margin=0.0):
        """Crop image according to the height of the spine."""
        xmin, ymin, xmax, ymax = self._roi()
        x_center = int((xmax + xmin) * 0.5)
        y_center = int((ymax + ymin) * 0.5)
        size = ymax - ymin
        msize = int((size * 0.5) + (size * t01l05_margin))
        return x_center - msize, y_center - msize, x_center + msize, y_center + msize

    def roi_square_spine(self, t01l05_margin=0.0):
        """Returns minimal ROI squared box centered on the spine and size according to spine length-> xmin, ymin, xmax, ymax"""
        xmin_centroids, ymin_centroids = np.min(self.centroids[:, :2], axis=0)
        xmax_centroids, ymax_centroids = np.max(self.centroids[:, :2], axis=0)

        spine_centroid = (0.5 * (
                    np.array([xmax_centroids, ymax_centroids]) + np.array([xmin_centroids, ymin_centroids]))).astype(
            int)

        roi_side_len = np.max([xmax_centroids - xmin_centroids, ymax_centroids - ymin_centroids])

        if abs(t01l05_margin) > 1e-6:
            pix_margin = np.linalg.norm(
                np.array([xmin_centroids, ymin_centroids]) - np.array([xmax_centroids, ymax_centroids])) * t01l05_margin
            roi_side_len += int(pix_margin)

        cursor = (0.5 * np.array([roi_side_len, roi_side_len])).astype(int)
        xmin, ymin = spine_centroid - cursor
        xmax, ymax = spine_centroid + cursor

        assert (xmax - xmin) == (ymax - ymin)

        return xmin, ymin, xmax, ymax

    def array_with_background_value(self, background_value=DEEP_VALUE):
        return np.where(np.isnan(self.values), background_value, self.values)

    def normalize_from_max(self, max_value=None):
        """Normalize depthmap from maximum value"""
        if max_value is None:
            max_value = np.nanmax(self.values)
        array = self.array_with_background_value(max_value)
        return array / max_value

    def normalized_depth_positions_from_background_max(self, max_value=None):
        """Normalize depthmap from maximum value"""
        if max_value is None:
            max_value = np.nanmax(self.values)
        positions = self.centroids.copy()
        positions[:, 2] = positions[:, 2] / max_value
        return positions

    def normalize(self):
        min_value = np.nanmin(self.values)
        max_value = np.nanmax(self.values)
        array = self.array_with_background_value(max_value)
        return (array - min_value) / np.ptp(array)

    def crop_to_roi(self, roi=None, t01l05_margin=0.0, reshape=True):
        if roi is None:
            roi = self.roi(t01l05_margin)

        array = self.values.copy()

        xmin = roi[0] if roi[0] >= 0 else 0 if roi[0] < self.values.shape[0] else self.values.shape[0] - 1
        ymin = roi[1] if roi[1] >= 0 else 0 if roi[1] < self.values.shape[1] else self.values.shape[1] - 1
        xmax = roi[2] if roi[2] < self.values.shape[0] else self.values.shape[0] - 1 if roi[2] >= 0 else 0
        ymax = roi[3] if roi[3] < self.values.shape[1] else self.values.shape[1] - 1 if roi[3] >= 0 else 0

        if reshape:
            res_array = array[ymin:ymax + 1, xmin:xmax + 1]
            landmarks = self.centroids[:, :2] - np.array([xmin, ymin])
        else:
            xrm = (roi[0] - xmin)
            yrm = (roi[1] - ymin)
            res_xmin = -1 * xrm if roi[0] < 0 else 0
            res_ymin = -1 * yrm if roi[1] < 0 else 0
            img_xlen = xmax - xmin
            img_ylen = ymax - ymin
            res_array = np.ones((roi[3] - roi[1] + 1, roi[2] - roi[0] + 1)) * np.max(array)
            res_array[res_ymin:res_ymin + img_ylen + 1, res_xmin:res_xmin + img_xlen + 1] = array[ymin:ymax + 1,
                                                                                            xmin:xmax + 1]
            landmarks = self.centroids.copy()
            landmarks[:, :2] = self.centroids[:, :2] - np.array([xmin - res_xmin, ymin - res_ymin])

        return res_array, landmarks


class Case():
    def __init__(self):
        self.id = ""
        self.back_depthmaps = []
        self.profile_depthmaps = []
        self.main_back_index = None

    @staticmethod
    def from_dir(dirpath: str, case_id: str, ndim=constants.POSITIONS_NDIM):
        c = Case()
        c.id = case_id
        array_regex = ARRAY_FILE_REGEX.format(c.id)
        npy_files = pathTools.getFilesMatchingPattern(dirpath, [array_regex], recursive_search=False)
        for p in npy_files:
            filename = commons.getFilename(p, "npy")
            if "_back_" in filename:
                c.back_depthmaps.append(Depthmap.from_npy(p, ndim=ndim))

        c.main_back_index = 0
        return c

    def back_depth_range(self):
        global_depthrange = [np.inf, -np.inf]
        for depthmap in self.back_depthmaps:
            depthrange = depthmap.depth_range()
            if global_depthrange[0] > depthrange[0]:
                global_depthrange[0] = depthrange[0]
            if global_depthrange[1] < depthrange[1]:
                global_depthrange[1] = depthrange[1]
        return global_depthrange

    def main_back_depthmap(self):
        if self.main_back_index is None:
            return None
        return self.back_depthmaps[self.main_back_index]


if __name__ == "__main__":
    case_id = "example"
    case_path = os.path.join("data", case_id)
    case = Case.from_dir(case_path, case_id)

    print("Number of depthmaps: {}".format(len(case.back_depthmaps)))

    main_dm = case.main_back_depthmap()

    print("  main back depthmap: {}".format(main_dm.array_path))

    fig, ax = plt.subplots()
    roi = main_dm.roi_square_spine(0.1)

    print("ROI: " + str(roi))

    array, positions = main_dm.crop_to_roi(roi, 0.1, reshape=False)
    new_shape = (224, 224)
    scale = np.array(new_shape) / np.array(array.shape)
    array = np.asarray(Image.fromarray(array).resize((224, 224), Image.BILINEAR))
    positions[:, :2] *= np.array(scale[::-1])
    print("Img dim: " + str(array.shape))
    ax.imshow(array, cmap="Greys")
    ax.autoscale(False)
    ax.scatter(positions[:, 0].tolist(), positions[:, 1].tolist())
    plt.show()
