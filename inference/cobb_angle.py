import numpy as np
import sys
import os
import numpy as np
import scipy.interpolate as interpolate

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import tools
import constants

# assume that x is the height (top-down) and y is the width (left-right or back-front)

MIN_NUMBER_OF_VERTEBRAE_INSIDE_COBB_ANGLE = 3

def spf(positions):
    return np.array(positions)[:, ::-1]


def fit_spline(positions, smooth=None, degree=3, extrapolate=True):
    p = np.array(positions).T
    x, y = p
    t, c, k = interpolate.splrep(x, y, s=smooth, k=degree)
    spline = interpolate.BSpline(t, c, k, extrapolate=extrapolate)
    return spline

def local_tangents(x, spline, dt=0.01):
    v = dt*np.array([1., spline.derivative()(x)])

    # normals
    v = np.array([-v[1], v[0]])
    v /= np.linalg.norm(v)

    return v, -1.*v


def local_tangent_at(i, vertebra_positions, spline, dt=0.01):
    p = vertebra_positions[i]
    x = p[0]
    tangent0, tangent1 = local_tangents(x, spline, dt)
    return tangent0, tangent1


def local_tangent_at_each_vertebra(vertebra_positions, spline, dt=0.01):
    tangents0 = list()
    tangents1 = list()
    for i in range(len(vertebra_positions)):
        tangent0, tangent1 = local_tangent_at(i, vertebra_positions=vertebra_positions, spline=spline, dt=dt)
        tangents0.append(tangent0)
        tangents1.append(tangent1)
    return tangents0, tangents1


def local_tangent_at_each_joint(vertebra_positions, spline, dt=0.01):
    tangents0 = list()
    tangents1 = list()
    for i in range(len(vertebra_positions)-1):
        vertSup = np.array(vertebra_positions[i])
        vertInf = np.array(vertebra_positions[i+1])
        ivp = (vertSup+vertInf)/2.0

        if i == 0:  # c07-t01 case
            #ivp_c07_t01 = vertSup - (ivp-vertSup)
            #tangent0, tangent1 = local_tangents(ivp_c07_t01[0], spline=spline, dt=dt)
            tangent0, tangent1 = local_tangents(vertSup[0], spline=spline, dt=dt)
            tangents0.append(tangent0)
            tangents1.append(tangent1)

        tangent0, tangent1 = local_tangents(ivp[0], spline=spline, dt=dt)
        tangents0.append(tangent0)
        tangents1.append(tangent1)

        if (i+1 == len(vertebra_positions)-1):
            #ivp_l05_s01 = vertInf + (vertInf-ivp)
            #tangent0, tangent1 = local_tangents(ivp_l05_s01[0], spline=spline, dt=dt)
            tangent0, tangent1 = local_tangents(vertInf[0], spline=spline, dt=dt)
            tangents0.append(tangent0)
            tangents1.append(tangent1)

    return tangents0, tangents1


def get_criteria_vertebra(vertebra_positions, vertebra_tangents=None):
    if vertebra_tangents is None:
        spline = fit_spline(positions=vertebra_positions)
        vertebra_tangents, _ = local_tangent_at_each_vertebra(vertebra_positions=vertebra_positions, spline=spline, dt=0.01)

    vp = np.array(vertebra_positions)
    vt = np.array(vertebra_tangents)

    criteria_vertebrae = list()

    for i in range(0, len(vp) - 2):
        # define upper, middle and lower
        upper = i
        middle = i + 1
        lower = i + 2

        # compute points of contact
        #contact_point_up = tools.intersection2d(vp[upper], vt[upper], vp[middle], vt[middle])
        #contact_point_lo = tools.intersection2d(vp[lower], vt[lower], vp[lower], vt[lower])

        alpha_up = tools.computeAngleBetween2DVectors(vt[upper], vt[middle])
        alpha_lo = tools.computeAngleBetween2DVectors(vt[middle], vt[lower])

        # get the side of the contact points
        lr_dim = 1
        contact_point_up_side = np.sign(alpha_up)
        contact_point_lo_side = np.sign(alpha_lo)

        if (contact_point_up_side != contact_point_lo_side):
            criteria_vertebrae.append(middle)

    return criteria_vertebrae


def criteria_vertebrae_selection(vertebra_positions, vertebra_tangents=None):
    if vertebra_tangents is None:
        spline = fit_spline(positions=vertebra_positions)
        vertebra_tangents, _ = local_tangent_at_each_vertebra(vertebra_positions=vertebra_positions, spline=spline, dt=0.01)

    vp = np.array(vertebra_positions)
    vt = np.array(vertebra_tangents)

    criteria_vertebrae = get_criteria_vertebra(vertebra_positions, vertebra_tangents=vertebra_tangents)

    true_criteria_vertebrae = list()
    for c in criteria_vertebrae:
        i = c + 1

        while (not i in criteria_vertebrae) and (i != (len(vp) - 1)):
            i += 1

        if (i - c) >= 3 or (i >= (len(vp)-1)):
            true_criteria_vertebrae.append(c)

    return true_criteria_vertebrae


# structure of a cobb angles list (ordered according to the absolute value of the cobb angle
#[
#   higher_cobb_angle, ..., lower_cobb_angle
#]

# structure of a cobb angle:
#[
#   |angle|, end_up, end_inf
#]
#
# end structure of a returned cobb angle (tangent orientation according to the sign of the ca):
#(
#   vertebra_index, end_position (x, y), end_tangent
#)
def cobb_angles_choi2017(vertebra_positions, dt=0.01, min_angle_degree=0.):
    #return cobb_angles_ours2022(vertebra_positions=vertebra_positions, dt=dt, min_angle_degree=min_angle_degree)
    #print(str("Use Choi CA method"))
    """From Choi 2017"""
    spline = fit_spline(positions=vertebra_positions)
    vp = np.array(vertebra_positions)
    vt, _ = local_tangent_at_each_vertebra(vertebra_positions=vertebra_positions, spline=spline, dt=dt)
    it, iti = local_tangent_at_each_joint(vertebra_positions=vertebra_positions, spline=spline, dt=dt)
    it = it[1:-1]
    iti = iti[1:-1]

    critera_vertebrae = criteria_vertebrae_selection(vp, vt)

    critera_vertebrae.sort()

    cobb_angles = list()
    values = list()
    for ci in range(len(critera_vertebrae)-1):
        cu = critera_vertebrae[ci]
        cl = critera_vertebrae[ci+1]

        iu = (vp[cu-1]+vp[cu])*0.5
        tu = it[cu-1]

        il = (vp[cl]+vp[cl+1])*0.5
        tl = it[cl]

        angle = np.degrees(tools.computeAngleBetween2DVectors(tu, tl))

        if np.abs(angle) > min_angle_degree:
            if angle > 0.:
                cobb_angles.append([np.abs(angle), (cu, iu, iti[cu-1]), (cl, il, iti[cl])])
            else:
                cobb_angles.append([np.abs(angle), (cu, iu, it[cu - 1]), (cl, il, it[cl])])

            values.append(np.abs(angle))

    cobb_angles = [x for _, x in sorted(zip(values, cobb_angles), reverse=True)]  # sort angles per severity

    return cobb_angles


def cobb_angle_(vertId0, vertId1, vertebra_positions, dt=0.01, on_vertebra_positions=False, signed_angle=False):
    i = constants.VERTEBRA_OF_INTEREST_IDS.index(vertId0)
    j = constants.VERTEBRA_OF_INTEREST_IDS.index(vertId1)

    spline = fit_spline(positions=vertebra_positions)
    vt, _ = local_tangent_at_each_joint(vertebra_positions=vertebra_positions, spline=spline, dt=dt)
    vp, _ = local_tangent_at_each_vertebra(vertebra_positions=vertebra_positions, spline=spline, dt=dt)

    vup = np.array(vp[i] if on_vertebra_positions else vt[i])
    vlow = np.array(vp[j] if on_vertebra_positions else vt[j+1])
    angle = np.degrees(tools.computeAngleBetween2DVectors(vup, vlow))

    if i != 0:
        if on_vertebra_positions:
            pup = vertebra_positions[i]
        else:
            pup = 0.5*(vertebra_positions[i-1] + vertebra_positions[i])
    else:
        pup = vertebra_positions[i]
        #pup = vertebra_positions[i] - 0.5*(vertebra_positions[i+1] - vertebra_positions[i])

    if j >= (len(vertebra_positions)-1):
        plow = vertebra_positions[j]
        #plow = vertebra_positions[j] + 0.5*(vertebra_positions[j] - vertebra_positions[j-1])
    else:
        if on_vertebra_positions:
            plow = vertebra_positions[j]
        else:
            plow = 0.5*(vertebra_positions[j] + vertebra_positions[j+1])

    ca = [angle if signed_angle else np.abs(angle), (i, pup, vup if angle < 0 else -1*vup), (j, plow, vlow if angle < 0 else -1*vlow)]

    return ca


def cobb_angle(vertId0, vertId1, vertebra_positions, dt=0.01, signed_angle=False):
    return cobb_angle_(vertId0, vertId1, vertebra_positions=vertebra_positions, dt=dt, signed_angle=signed_angle)[0]
