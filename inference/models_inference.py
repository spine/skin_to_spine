import constants
import sys
import os
import numpy as np
import torch
import torchvision.transforms.functional as TF
from PIL import Image
from torch.utils.data import Dataset
import torch.nn.functional as F
import ctypes
import inspect
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from scipy import ndimage
from scipy.spatial.transform import Rotation as R

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import constants

def dataset_positions_to_matrix(dataset):
    X = list()
    y = list()
    for i in range(len(dataset)):
        positions = dataset[i][1].numpy()
        X.append(positions.flatten().tolist())
        y.append(i)
    return np.array(X), np.array(y)


def dataset_to_PCA_input(dataset, scaler):
    X, y = dataset_positions_to_matrix(dataset)
    if scaler is None:
        return np.array(X), np.array(y)
    return scaler.transform(np.array(X)), np.array(y)


def dataset_from_PCA_input(X, scaler=None, ndim=constants.POSITIONS_NDIM):
    res = list()
    if scaler is None:
        X_s = X
    else:
        X_s = scaler.inverse_transform(X)
    for p in range(len(X_s)):
        coordinates = X_s[p]
        res.append(np.reshape(coordinates, (17, ndim)))
    return np.array(res)

def reduced_space_to_raw_positions(x, pca, scaler, ndim):
    r = pca.inverse_transform(x)
    return dataset_from_PCA_input(r, scaler, ndim=ndim)

def predict_positions_pca_from_depthmap(image, network, pca, scaler, ndim, device=constants.device):
    img = torch.stack([TF.to_tensor(Image.fromarray(image))])
    img = img.float().to(device)
    with torch.no_grad():
        predictions = network(img).float().to(device)
        predictions = predictions.view(-1, pca.n_components_, 1)
        predictions = reduced_space_to_raw_positions([predictions.cpu()[0].numpy().flatten()], pca, scaler=scaler, ndim=ndim)[0]
    return predictions