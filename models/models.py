import os
import sys
import torch.nn as nn
import torch
from torchvision import models as tmodels

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import constants

def get_models(learning_state: str, device=constants.device):
    exp_name = os.path.basename(learning_state)
    cnn_model_path = os.path.join(learning_state, "cnn_model.pth")

    pca_model_path = os.path.join(learning_state, "pca_model.pkl")
    scaler_model_path = os.path.join(learning_state, "pca_scaler.pkl")

    pca = None
    scaler = None

    if os.path.exists(pca_model_path):
        import pickle as pk
        pca = pk.load(open(pca_model_path, 'rb'))

        if os.path.exists(scaler_model_path):
            scaler = pk.load(open(scaler_model_path, 'rb'))

    image_size = (224, 224)

    network = ResNet18(pca.n_components_).float().to(device)

    with torch.no_grad():
        if not os.path.exists(cnn_model_path):
            print("Warning model doesn't exist at {}".format(cnn_model_path))

        if os.path.exists(cnn_model_path):
            network.load_state_dict(torch.load(cnn_model_path))
            if not os.path.exists(cnn_model_path):
                print("Warning model doesn't exist at {}".format(cnn_model_path))
        else:
            cnn_model_path = os.path.join(learning_state, "checkpoint.pt")
            if not os.path.exists(cnn_model_path):
                print("Model doesn't exist in {}".format(cnn_model_path))
            else:
                checkpoint = torch.load(cnn_model_path)
                network.load_state_dict(checkpoint['model_state_dict'])

    output_length = network.num_classes

    return network, \
           scaler, \
           pca, \
           cnn_model_path, \
           scaler_model_path, \
           pca_model_path, \
           output_length, \
           image_size, \
           device

class Identity(nn.Module):
    def __init__(self, *args, **kwargs):
        super().__init__()
    def forward(self, x):
        return x

class ResNet18(nn.Module):
    def __init__(self, num_classes):
        super().__init__()
        self.num_classes = num_classes
        self.model = tmodels.resnet18()
        self.model_name = 'resnet18'
        self.model.conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.model.fc = nn.Linear(self.model.fc.in_features, self.num_classes)

    def forward(self, x):
        x = self.model(x)
        return x
