from __future__ import absolute_import

import os

import asserter


def getFiles(path, file_extensions=[], recursive_search=False):
    """List of files. Files can be selected using a list of extensions -> list"""
    res = list()
    if not os.path.isdir(path):
        return res

    def getFiles_core(root, filenames, directories):
        for f in filenames:
            file_path = os.path.join(root, f)
            if (f.split(".")[-1] in file_extensions) or len(file_extensions) == 0:
                res.append(file_path)

    if recursive_search:
        runRecursive(path, getFiles_core)
    else:
        root, files, dirs = dirContent(path)
        getFiles_core(root, filenames=files, directories=dirs)

    return res


def getFilesMatchingPattern(path: str, patterns: list, recursive_search: bool = False) -> list:
    """List of files. Files are selected accroding to a pattern in path -> list"""
    import commons
    res = list()
    if not os.path.isdir(path):
        return res

    def getFilesMatchingPattern_core(root, filenames, directories):
        for f in filenames:
            file_path = os.path.join(root, f)
            for pattern in patterns:
                if len(commons.findExpression(pattern, file_path)[0]) != 0:
                    res.append(file_path)
                    break

    if recursive_search:
        runRecursive(path, getFilesMatchingPattern_core)
    else:
        root, files, dirs = dirContent(path)
        getFilesMatchingPattern_core(root, filenames=files, directories=dirs)

    return res


def getDirectoriesMatchingPattern(path: str, patterns: list, recursive_search: bool = False) -> list:
    """List of directories. Directories are selected according to a pattern in path -> list"""
    import commons
    res = list()
    if not os.path.isdir(path):
        return res

    def getDirectoriesMatchingPattern_core(root, filenames, directories):
        for d in directories:
            dir_path = os.path.join(root, d)
            for pattern in patterns:
                if len(commons.findExpression(pattern, dir_path)[0]) != 0:
                    res.append(dir_path)
                    break

    if recursive_search:
        runRecursive(path, getDirectoriesMatchingPattern_core)
    else:
        root, files, dirs = dirContent(path)
        getDirectoriesMatchingPattern_core(root, filenames=files, directories=dirs)

    return res


def dirContent(root):
    """Returns lists of a directory content: directory path, files, sub-directories -> list, list, list"""
    content = os.listdir(root)
    files = list()
    dirs = list()
    for e in content:
        path = os.path.join(root, e)
        if os.path.isdir(path):
            dirs.append(e)
        else:
            files.append(e)

    return root, files, dirs


def executeFunc(root, func, files, dirs):
    """Model of the function func."""
    return func(root=root, filenames=files, directories=dirs)


def runRecursive(root, func):
    """ Func is a function that manages three inputs: root (str), filenames (list) and directories (list).
        Example:
            def printContent(root, filenames, directories):
                print("Directory " + str(root) + " contains:")
                if len(filenames) > 0:
                    print("\t* files: " + ", ".join(filenames))
                if len(directories) > 0:
                print("\t* directories: " + ", ".join(directories))
    """
    _, files, dirs = dirContent(root)
    for directory in dirs:
        runRecursive(os.path.join(root, directory), func)

    executeFunc(root, func, files, dirs)


def removePath(path):
    """Remove path. Remove recursively each file and directory inside path."""
    if os.path.exists(path):
        # first of all remove files
        for root, dirs, files in os.walk(path, topdown=False):
            for f in files:
                os.remove(os.path.join(root, f))
            for d in dirs:
                os.rmdir(os.path.join(root, d))

        os.rmdir(path)
    else:
        print("Path " + str(path) + " does not exist.")
        raise ValueError


def retrieveDirPaths(root, deepSearch=True, dataTypes=["obj", "stl"]):
    """Search input directories with data -> list"""
    inputDirs = list()

    for dir, subdirs, subfiles in os.walk(root, topdown=True):
        hasFiles = False
        dirContents = os.listdir(os.path.join(dir))
        for dirContent in dirContents:
            fileCandidate = os.path.join(dir, dirContent)
            if os.path.isfile(fileCandidate) and ((fileCandidate.split(".")[-1] in dataTypes) or (len(dataTypes) == 0)):
                # if the directory contains files with the correct filename extension or dataType is empty
                hasFiles = True

        if hasFiles:
            inputDirs.append(os.path.join(dir))

        if not deepSearch:
            break

    return inputDirs
