import numpy as np

def normalizeImgAB(img, min=-1., max=1.):
    img_min = np.nanmin(img)
    img_max = np.nanmax(img)
    res = ((max-min)*(img - img_min)/(img_max - img_min)) + min
    return res


def minmax(x, ignorenan=True):
    x_ = np.array(x)
    if ignorenan:
        x_ = x_[~np.isnan(x_)]
    maximum = x_[0]
    minimum = x_[0]
    for i in x_[1:]:
        if i > maximum:
            maximum = i
        elif i < minimum:
            minimum = i
    return (minimum, maximum)


def computeAngleBetween2DVectors(v0, v1):
    v0 = v0 / np.linalg.norm(v0)
    v1 = v1 / np.linalg.norm(v1)

    # compute angle between v0 and v1
    dot_product = np.dot(v0, v1)
    angle = np.arccos(dot_product)

    # compute cross product between v0 and v1
    sign = np.sign(np.linalg.det(np.asarray([v0, v1])))

    return sign * angle